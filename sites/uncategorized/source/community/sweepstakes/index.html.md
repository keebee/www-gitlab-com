---
layout: markdown_page
title: "GitLab Promotional Games"
description: "View a full listing of current and previous GitLab Promotional Games. Find more information here!"
canonical_path: "/community/sweepstakes/"
---
<!-- Housekeeping: Please deprecate any Promotional Game drawn more than 180 days ago. -->
## Current and previous promotional games

### Current
- [GitLab IT Revolution iPad Sweepstakes](https://about.gitlab.com/community/sweepstakes/gitlab-it-revolution-ipad-sweepstakes/)

### Past
- [GitLab Survey weekly digest 2022](/community/sweepstakes/2022-survey-weekly-digest/survey-weekly-digest.index.html)

